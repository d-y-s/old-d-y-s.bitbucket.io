$('.partners__items').owlCarousel({
    items: 5,
    loop: true,
    nav: true,
    dots: false,
    margin: 0,
    smartSpeed: 800,
    navText: ['', ''],
    responsive: {
        320: {
            items: 2
        },
        640: {
            items: 3
        },
        800: {
            items: 4
        },
        1024: {
            items: 5
        },
        1367: {
            items: 5
        }
    }
})