$('.form-popup__form').on('submit', function (event) {
    var $this = $(this);
    event.preventDefault();
    $this.closest('.form-popup__section').addClass('success');
    $this.closest('.form-popup').find('.form-popup__send-success').addClass('success');
    setTimeout(function () {
        $this.closest('.app').find('.remodal-overlay').css('display', 'none');
        $this.closest('.app').find('.remodal-wrapper').css('display', 'none');
        $('html').removeClass('remodal-is-locked');
        $('body').css('padding-right', 0)
    }, 3000);
    setTimeout(function () {
        $this.closest('.form-popup__section').removeClass('success');
        $this.closest('.form-popup').find('.form-popup__send-success').removeClass('success');
    }, 3000);
  });