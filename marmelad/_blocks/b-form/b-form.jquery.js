$('.b-form__form').on('submit', function (event) {
    var $this = $(this);
    event.preventDefault();
    $this.closest('.b-form__section').addClass('success');
    $this.closest('.b-form').find('.b-form__send-success').addClass('success');

    setTimeout(function () {
        $this.closest('.b-form__section').removeClass('success');
        $this.closest('.b-form').find('.b-form__send-success').removeClass('success');
    }, 8000);
});