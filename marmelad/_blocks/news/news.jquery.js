$('.news__items').owlCarousel({
    items: 2,
    loop: true,
    nav: true,
    dots: false,
    margin: 30,
    smartSpeed: 800,
    navText: ['', ''],
    responsive:{
        320:{
            items:1
        },
        768:{
            items:2
        }
    }
})