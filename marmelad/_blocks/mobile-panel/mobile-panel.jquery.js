var topNav = $('.top-nav'),
    topNavUl = $('.top-nav').find('.top-nav__menu'),
    mobilePanelItemNav = $('.mobile-panel__item--for-nav'),
    mobilePanelNav = $('.mobile-panel__nav');

// var liItem = $('.top-nav .top-nav__menu li'),
//     subMenu = liItem.find('ul'),
//     subMenuItem = subMenu.parent().addClass("sub-menu-item");

function appWindowSize() {

    if ($(window).width() <= 1100) {
        topNavUl.appendTo(mobilePanelNav);
    }
    else {
        topNavUl.appendTo(topNav);
    }

}

$('.mobile-panel__burger-btn').on('click', function() {
    mobilePanelNav.slideToggle();
});

// topNavUl.on('click', '.arrow-sub-menu', function() {    
//     $(this).siblings('ul').slideToggle();
//     $(this).parent(subMenuItem).toggleClass('active');
// });





$(document).on('click', function(e) {
    if($(e.target).closest('.mobile-panel').length){
    return;
    }
    mobilePanelNav.slideUp();
});

$(window).on('load resize', appWindowSize);
