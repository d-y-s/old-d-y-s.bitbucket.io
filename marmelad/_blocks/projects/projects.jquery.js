$('.projects__items').owlCarousel({
    items: 4,
    loop: true,
    nav: true,
    dots: false,
    rewind: false,
    smartSpeed: 800,
    margin: 200,
    navText: ['', ''],
    responsive: {
        320: {
            items: 1,
            margin: 0
        },
        640: {
            items: 2,
            margin: 50
        },
        800: {
            items: 3,
            margin: 30
        },
        1024: {
            items: 3,
            margin: 50
        },
        1367: {
            items: 4,
            margin: 200
        }
    }
});

$('.projects__items .owl-item').not('.cloned').each(function (i, el) {
    var $this = $(this),
        itemCount = $this.find('.projects__count');
    
    if ($(window).width() >= 1024 && $(window).width() <= 1366) {
    
        itemCount.text($this.index() - 2);
    
    }

    if ($(window).width() >= 1367) {
    
        itemCount.text($this.index() - 3);
    
    }

    // itemCount.text($this.index() - 3);
});