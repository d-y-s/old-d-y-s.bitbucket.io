$('.our-products__items').owlCarousel({
    items: 4,
    loop: true,
    nav: true,
    dots: false,
    smartSpeed: 800,
    navText: ['', ''],
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 2
        },
        960: {
            items: 3
        },
        1279: {
            items: 4
        }
    }
});