/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */
//=require ../_blocks/**/_*.js


/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function() {

    'use strict';

    /**
     * определение существования элемента на странице
     */
    $.exists = (selector) => $(selector).length > 0;

    /**
     * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
     */
    //=require ../_blocks/**/[^_]*.jquery.js

    // маска телефона
    $('.phone_us').mask('+7 (000) 000-00-00');

    // маска e-mail
    $(".e-mail_mask").inputmask("email")

    function moreText() {
            
        if ($(window).width() <= 480) {

            $('.content-text__section').readmore({
                speed: 300,
                maxHeight: 185,
                moreLink: "<span calss='content-text__js-more-text-btn'>Читать полностью</span>",
                lessLink: "<span calss='content-text__js-more-text-btn'>Свернуть текст</span>"
            });
            
        }

    }
      
    $(window).on('load', moreText);

    
});
