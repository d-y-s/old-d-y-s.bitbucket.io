module.exports = {
  app: {
    lang: 'ru',
    stylus: {
      theme_color: '#3E50B4',
    },
    GA: false, // Google Analytics site's ID
    package: 'ключ перезаписывается значениями из package.json marmelad-сборщика',
    settings: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    storage: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    buildTime: '',
    controls: [
      'default',
      'brand',
      'success',
      'info',
      'warning',
      'danger',
    ],
  },

  servicesItem: [
    {
      'icon': 'services-icon.png',
      'name': 'Разработка и экспертиза сметной документации'
    },
    {
      'icon': 'services-icon2.png',
      'name': 'Разработка сметных норм и расценок'
    },
    {
      'icon': 'services-icon3.png',
      'name': 'Оказание образовательных услуг'
    },
    {
      'icon': 'services-icon4.png',
      'name': 'Осуществление консалтинговых услуг'
    },
    {
      'icon': 'services-icon5.png',
      'name': 'Проведение независимого мониторинга стоимости строительных ресурсов'
    },{
      'icon': 'services-icon6.png',
      'name': 'Разработка индексов изменения сметной стоимости'
    }
  ],

  servicesItem__inner: [
    {
      'img': 'inner-page-img.jpg',
      'name': 'Формирование и проверка сметной документации '
    },
    {
      'img': 'inner-page-img2.jpg',
      'name': 'Разработка сметных нормативов, индексов и методических документов'
    },
    {
      'img': 'inner-page-img3.jpg',
      'name': 'Организация включения в Классификатор строительных ресурсов (КСР) продукции '
    },
    {
      'img': 'inner-page-img4.jpg',
      'name': 'Сопровождение в органах экспертизы'
    },
    {
      'img': 'inner-page-img5.jpg',
      'name': 'Проведение независимого мониторинга цен'
    },{
      'img': 'inner-page-img6.jpg',
      'name': 'Осуществление консалтинговых и образовательных услуг'
    }
  ],

  ourProducts: [
    {
      'img': 'our-products-pic.png',
      'name': 'Система мониторинга цен строительных ресурсов'
    },
    {
      'img': 'our-products-pic2.png',
      'name': 'Сборники цен строительных ресурсов и видов работ'
    },
    {
      'img': 'our-products-pic3.png',
      'name': 'Индексы изменения сметной стоимости строительства'
    },
    {
      'img': 'our-products-pic4.png',
      'name': 'Журнал о ценообразовании и нормировании в сфере строительства «СметаНа»'
    },
    {
      'img': 'our-products-pic3.png',
      'name': 'Индексы изменения сметной стоимости строительства'
    },
  ],

  projectsitem: [
    {
      'img': 'projects-img.jpg',
      'name': 'Длинное название проекта'
    },
    {
      'img': 'projects-img2.jpg',
      'name': 'Название проекта'
    },
    {
      'img': 'projects-img3.jpg',
      'name': 'Очень длинное название проекта в три строки'
    },
    {
      'img': 'projects-img4.jpg',
      'name': 'Длинное название проекта'
    },
    {
      'img': 'projects-img3.jpg',
      'name': 'Очень длинное название проекта в три строки'
    }
  ],

  partnersItem: [
    {
      'img': 'partners-img.png',
    },
    {
      'img': 'partners-img2.png',
    },
    {
      'img': 'partners-img3.png',
    },
    {
      'img': 'partners-img4.png',
    },
    {
      'img': 'partners-img5.png',
    },
    {
      'img': 'partners-img3.png',
    }
  ],

  socialLinks: [
    {
      'url': 'https://www.instagram.com',
      'icon': 'inst.png'
    },
    {
      'url': 'https://www.facebook.com',
      'icon': 'fb.png'
    },
    {
      'url': 'https://twitter.com',
      'icon': 'tw.png'
    },
    {
      'url': 'https://www.vk.com',
      'icon': 'vk.png'
    }
  ],

  crumbs: {
    servicesPage : [
        {
            title : "Главная",
            url  : "#"
        },
        {
            title : "Услуги",
            url  : "#"
        }
    ]
},

  pageTitle: 'marmelad',
};
